package ht.stage.ckwa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CkwaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CkwaApplication.class, args);
	}
}
