package ht.stage.ckwa.controller;

import ht.stage.ckwa.model.Product;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    private ArrayList<Product> products;
    private int nextId;

    public ProductController() {
        products = new ArrayList<>();
        products.add(new Product(1, "Laptop", "l1", 2, 4.5));
        products.add(new Product(2, "Souris", "s1", 0.5, 1.5));
        nextId = 3;
    }

    private Product findProduct(int id) {
        for (Product product :
                products) {
            if (product.getId() == id) {
                return product;
            }
        }
        return null;
    }

    @GetMapping
    public ArrayList<Product> index() {
        return products;
    }

    @PostMapping
    public Product store(@RequestBody Product product) {
        product.setId(nextId);
        products.add(product);
        nextId++;
        return product;
    }

    @GetMapping("{id}")
    public Product show(@PathVariable("id") int id) {
        return findProduct(id);
    }

    @PutMapping("{id}")
    public Product update(@PathVariable("id") int id, @RequestBody Product product) {
        Product oldProduct = findProduct(id);
        if (oldProduct != null) {
            oldProduct.setCode(product.getCode());
            oldProduct.setName(product.getName());
            oldProduct.setWeight(product.getWeight());
            oldProduct.setSize(product.getSize());
            return oldProduct;
        }
        return null;
    }

    @DeleteMapping("{id}")
    public int destroy(@PathVariable("id") int id) {
        Product product = findProduct(id);
        if (product != null) {
            products.remove(product);
            return product.getId();
        }
        return -1;
    }
}
