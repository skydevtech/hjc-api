package ht.stage.ckwa.model;

public class Product {
    private int id;
    private String name;
    private String code;
    private double size;
    private double weight;

    public Product() {
    }

    public Product(int id, String name, String code, double size, double weight) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.size = size;
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
